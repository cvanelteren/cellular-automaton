# Cellular automaton simulation
With added 2D support using Moore neighborhood
## Usage
call from command line with format
```python
python CellularAutomaton.py --[command] value
```
- Possible commands
    - step
    - size
    - rule

# Example
By running:
```python
python CellularAutomaton.py --rule -1
```
will generate an example of a subset of rules

![Showcase](ShowCase.png)
