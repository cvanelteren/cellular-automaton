'''
@author: Casper van Elteren

'''
from pylab import *
from numpy import *
import itertools
from tqdm import tqdm
class CA:
    '''
    Class representing a elementary cellular automaton
    '''
    def __init__(self, N = (10,)):
        if type(N) is int:
            N = (N,)
        self.N = N
        self.coordinates = list(itertools.product(*[list(range(i)) for i in N]))
        self.nDim = len(N)
        self.nNeighbors = 3 ** self.nDim
        # use archimedial spiral for looping through 2D matrices
        thetas = linspace(0, 2*pi, self.nNeighbors)
        spiral = thetas * exp(-1j * thetas)
        self.spiral = stack((spiral.real, spiral.imag)).round().clip(-1,1).T # clip the values
        # find center and make sure it is in the center
        if self.nDim < 2:
            self.spiral = unique(self.spiral)[:,None]
        print('Spiral \t', self.spiral)

    def simulate(self, steps, rule, init = []):
        '''
        Simulates for n steps the elementary cellular automaton
        '''
        if init == []:
            self.states = random.randint(0,2, size = self.N)[..., None] # init with 2 dimensions
        else:
            self.states = init
        # TODO: misschien dit deel naar boven verplaatsen in init
        self.dimensions = self.states.shape
        # set the rule hashmap
        self.setRules(rule)
        print('Busy on rule {0}'.format(rule))
        for step in tqdm(range(steps)):
            self.update()
        # allow for multiple storages [extenernal]
        return self.states[..., -1] if len(self.states.shape) > 2 else self.states

    def setRules(self, rule):
        '''
        Constructs a hashmap of the rule set used in the automaton
        '''
        # compact way of writing every combination of triplet
        # the format $00b reads as; format i as binary positive number using 5 digits
        # the first 2 digits are sign bits
        # convert dec number to 8 digit binary
        number = [int(i) for i in format(rule, '0{0}b'.format(2**self.nNeighbors))[::-1]] # smart trick from @brahmsapoor
        self.rule = number
        # OLD:
        # self.rules = {}
        # for counter, assignment in enumerate(number):
        #     # create a combination of triplet, i.e. 0 0 0, 1 1 1 etc
        #     combination = tuple([int(i) for i in format(counter, '0{0}b'.format(self.nNeighbors))])
        #     self.rules[combination] = assignment

    def getNeighbours(self, currentState, coordinate):
        ''''
        Based on a coordinate within the state matrix, gets all the possible
        combinations of the neighbours. Essentially this returns a cropped submatrix
        '''
        # construct a neighbour list along each dimension, except for the last (which contains the samples)
        tmp = []
        # compute angle and use archimedial spiral to get the correct
        # squence of the submatrix
        # spiral can be precomputed
        coordinate = array(coordinate)
        neighbours = []
        for shift in self.spiral:
            tmp = shift.copy() # make copy of the shift
            tmp += coordinate # move in direction ; this is the index in the grid
            # for every dimension check if boundary is exceeded
            for idx in range(self.nDim):
                tmp[idx] = tmp[idx] % self.dimensions[idx]
            tmp = tuple(tmp) + (0,)     # add for compatibility ; dimension is a vector
            tmp  = tuple(map(int, tmp)) # sanity check
            # print(tmp, currentState.shape, coordinate)
            # print(tmp)
            neighbours.append(currentState[tmp])

        return neighbours
    def determineState(self, neighbours):
        '''
        Calculates the state of the middle node in the next generation
        '''
        idx = int(sum([2**i if j == 1 else 0 for i, j in enumerate(neighbours[::-1])]))
        return int(self.rule[idx])
        # return self.rules[tuple(neighbours)]
    def update(self):
        '''
        Perform the update from generation to the next
        '''
        # get the last generation
        newStates = []
        # loop through the cells
        currentState = self.states[..., [-1]]
        for coordinate in self.coordinates:
            neighbours = self.getNeighbours(currentState, coordinate)
            newState   = self.determineState(neighbours)
            newStates.append(newState)
        # append new state to the stack
        newStates = array(newStates).reshape(*self.N)[..., None]
        # print('?', newStates.shape, self.states.shape)
        # print(newStates)
        # ugly workaround
        if self.nDim < 2:
            self.states = hstack((self.states, newStates))
        else:
            self.states = dstack((self.states, newStates))
    def show(self, data = None):
        if data is None:
            w = self.states[...,-1] if len(self.states.shape) > 2 else self.states
            fig, ax = subplots()
            h = ax.imshow(w, aspect = 'auto', cmap = 'gray_r')
            setp(ax, **{'xlabel': 'Cell', 'ylabel':'Generation'})
            h = colorbar(h, ax = ax)
            show()
        else:
            # even column spread
            nColumns = len(data) // 2
            nRows    = len(data) // nColumns
            fig, axs = subplots(nColumns, nRows, sharex = 'all', sharey = 'all')
            cmap = cm.get_cmap('gray_r', 2)
            for ax, datai in zip(axs.flatten(), data.items()):
                rule, plotData = datai
                h = ax.imshow(plotData, aspect = 'auto', cmap = cmap)
                ax.set_title(rule)
            # add general frame to have centered label
            ax = fig.add_subplot(1,1,1, axisbg = (0,0,0,0)) # remove plot bg
            # remove axes and add centered labelss
            setp(ax, xlabel = 'Cell', ylabel = 'Generation', xticks = [], yticks = [])
            # remove borders of the plot generated by matplotlib
            for spine in gca().spines.values():
                spine.set_visible(False)
            # pad labels for visibility
            # colorbar(h, ax = ax)
            ax.xaxis.labelpad = 20; ax.yaxis.labelpad = 30
            # subplots_adjust(right = .9)
            savefig('ShowCase.png') # save showcase
            show()
if __name__ == '__main__':
    import argparse
    # DEFAULT PARAMETERS
    N    = (50,50 )
    nT   = 22
    rule = 60
    getTuple = lambda x: [int(i) for i in x.split(',')]
    # parser environment
    parser = argparse.ArgumentParser("Cellular automaton")
    parser.add_argument("--rule", help="Cellular automaton rule to simulate", type=int, default = rule)
    parser.add_argument('--size', help = 'Network size', type = getTuple, default = N)
    parser.add_argument('--step', help = 'number of times to simulate', type = int, default = nT)
    args = parser.parse_args()

    # print some output
    print('Rule\t{0}\nSize\t{1}\nSteps\t{2}'.format(args.rule, args.size, args.step))
    # the default used by wolfram
    tmp = zeros((*args.size, 1))
    tmp[tuple((i//2 for i in tmp.shape))] = 1
    # init automaton
    automaton = CA(args.size)
    if args.rule == -1:
        rules = [30, 54, 120, 110]
        data  = {}
        for rule in rules:
            data[rule] = automaton.simulate(args.step, rule, tmp).T
            print(data[rule].shape)
        automaton.show(data)
    else:
        automaton.simulate(args.step, args.rule, tmp)
        automaton.show()
